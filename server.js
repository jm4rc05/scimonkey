#!/usr/bin/env node

require('app-module-path').addPath(__dirname + '/helpers')

const path = require('path')
const config = require('config')
const keystone = require('keystone')

process.on('SIGINT', function() {
    console.log('\nExiting...')
    process.exit()
})

keystone.init(config.cms)

keystone.import(config.models)
keystone.set('nav', { users: ['users'] })
keystone.set('routes', require(path.join(__dirname, config.routes)))

keystone.set('400', function(req, res) {
    res.status(400);
    res.render('error.handlebars', { layout: 'error', message: 'Requisição inválida' });
});

keystone.set('403', function(req, res) {
    res.status(403);
    res.render('error.handlebars', { layout: 'error', message: 'Acesso proibido' });
});

keystone.set('404', function(req, res) {
    res.status(404);
    res.render('error.handlebars', { layout: 'error', message: 'Página não encontrada' });
});

keystone.set('405', function(req, res) {
    res.status(405);
    res.render('error.handlebars', { layout: 'error', message: 'Método não permitido' });
});

keystone.set('408', function(req, res) {
    res.status(408);
    res.render('error.handlebars', { layout: 'error', message: 'Tempo de resposta excedido' });
});

keystone.set('500', function(error, req, res, next) {
    res.status(500);
    res.render('error.handlebars', { layout: 'error', message: 'Erro interno do servidor', error: error });
});

keystone.set('502', function(error, req, res, next) {
    res.status(502);
    res.render('error.handlebars', { layout: 'error', message: 'Erro intermediando resposta da aplicação', error: error });
});

keystone.set('504', function(error, req, res, next) {
    res.status(504);
    res.render('error.handlebars', { layout: 'error', message: 'Tempo de espera da resposta da aplicação excedido', error: error });
});

keystone.start()