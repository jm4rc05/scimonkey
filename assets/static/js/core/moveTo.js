function moveTo(contentArea) {
    var goPosition = $(contentArea).offset().top;
    $('html,body').animate({
        scrollTop: goPosition
    }, 'slow');
}