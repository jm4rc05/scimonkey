$(window).load(function() {
    $("nav").sticky({
        topSpacing: 0,
        className: 'sticky',
        wrapperClassName: 'my-wrapper'
    });
});