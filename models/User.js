var async = require('async')
var keystone = require('keystone')
var Email = require('keystone-email')
var fileManager = require('keystone-file-manager')
var intl = require('mongoose-intl')
var Types = keystone.Field.Types

var User = new keystone.List('User', {
    map: { name: 'displayName' },
    track: true,
})

new fileManager(User).init()

User.schema.plugin(intl, { languages: ['en', 'pt'], defaultLanguage: 'pt' })

User.add({
    displayName: { type: String, unique: true, index: true },
    password: { type: Types.Password },
    email: { type: Types.Email, unique: true, index: true },
    isAdmin: { type: Boolean, default: false },
    fullName: { type: String },
})

User.schema.virtual('canAccessKeystone').get(function() {
    return this.isAdmin ? true : false
})

User.defaultColumns = 'id, displayName, email, isAdmin'

User.register()