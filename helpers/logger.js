const appRoot = require('app-root-path')
const winston = require('winston')

require('winston-daily-rotate-file')

const options = {
    file: {
        level: 'info',
        filename: `${appRoot.path}/log/log-%DATE%.json`,
        datePattern: 'YYYY-MM-DD-HH',
        zippedArchive: true,
        maxSize: '20m',
        maxFiles: '14d',
        handleExceptions: true,
        json: true,
        colorize: false,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
}

var logger = new winston.createLogger({
    transports: [
        new winston.transports.DailyRotateFile(options.file),
        new winston.transports.Console(options.console)
    ],
    exitOnError: false,
})

logger.stream = {
    write: function(message, encoding) {
        logger.info(message);
    },
}

module.exports = logger