const _ = require('lodash')

require('dotenv').config()

console.info('Reading configuration...')

const defaults = require('../config/default.js')
const env = require('../config/' + ((process.env.NODE_ENV === undefined) ? 'production' : process.env.NODE_ENV) + '.js')

const config = _.merge({}, defaults, env)

module.exports = config