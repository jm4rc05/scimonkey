Criar arquivo `.env` contendo

```
NODE_ENV=development
```

Onde `dev` é o nome do arquivo `config\development.js`.

Opcionalmente substituir `dev` por `production` ou `test` de acordo com o ambiente desejado (ver arquivos na pasta `config`).

## Setup mínimo do *mongodb*

```
use portal
db.createUser({ user: "portalAdmin", pwd: "p4ssw04d", roles: [ "dbOwner" ]})
```
