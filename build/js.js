const gulp = require('gulp')
const minify = require('gulp-minify')

gulp.task('js', function() {
    return gulp.src(['assets/static/js/**/*.js'])
        .pipe(minify({
            ext: {
                min: '.min.js'
            },
            noSource: true
        }))
        .pipe(gulp.dest('public/assets/static/js'))
})