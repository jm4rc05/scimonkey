const gulp = require('gulp')
const cleanCss = require('gulp-clean-css')
const rename = require('gulp-rename')

gulp.task('css', function() {
    return gulp.src(['assets/static/css/*.css'])
        .pipe(cleanCss())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('public/assets/static/css'))
})