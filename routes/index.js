const keystone = require('keystone')
const config = require('config')
const headers = require('helmet')
const csp = require('helmet-csp')
const expectCT = require('expect-ct')
const crypto = require('crypto')

const routeImporter = keystone.importer(__dirname)

const routes = {
    views: routeImporter('views'),
    // methods: routeImporter('methods'),
}

exports = module.exports = function(app) {
    app.use(function(req, res, next) {
        const nonce = crypto.randomBytes(16).toString('hex')

        config.policies.directives.scriptSrc = [
            "'strict-dynamic'",
            `'nonce-${nonce}'`,
            "'unsafe-inline'",
            "http:",
            "https:",
        ]

        res.locals.nonce = nonce

        csp(config.policies)

        headers.hsts(config.hsts)
        headers.xssFilter()
        headers.noSniff()
        headers.featurePolicy(config.featurePolicy)
        expectCT(config.expectCT)

        next()
    })

    app.get('/', [keystone.middleware.api, keystone.middleware.cors], routes.views.home)
    app.get('/blog', [keystone.middleware.api, keystone.middleware.cors], routes.views.blog)
    app.get('/post', [keystone.middleware.api, keystone.middleware.cors], routes.views.post)
}