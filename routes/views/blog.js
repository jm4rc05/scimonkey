require('app-module-path').addPath(__dirname + '/helpers')

const keystone = require('keystone')

module.exports = function(req, res) {
    const view = new keystone.View(req, res)
    locals = res.locals

    locals.config = require('config')

    locals.data = {
        today: new Date(),
    }

    view.render('blog', { layout: 'blog' })
}