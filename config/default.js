const modPath = require('app-module-path').addPath(__dirname + '/helpers')

const engine = require('express-handlebars')
const handlebarsHelpers = require('handlebars-helpers')()
const templateHelpers = require('template-helpers')()
const helperMoment = require('helper-moment')

const logger = require('logger')

const UPLOADS = './public/uploads'

module.exports = {
    storage: {
        uploads: UPLOADS,
    },
    cms: {
        'name': 'scimonkey',
        'brand': 'scimonkey',
        'static': 'assets',
        'logo': '/static/images/logo.png',
        'compress': true,

        'auto update': true,
        'user model': 'User',

        'port': 3001,
        'session': true,
        'session store': 'mongo',
        'auth': true,
        'admin path': 'admin',
        'signin logo': '/static/images/logo-ita.png',

        'logger': 'combined',
        'logger options': {
            stream: logger.stream
        },

        'kfm public url': '/static/uploads/',
        'kfm uploaded files storage': UPLOADS,

        'views': 'views',

        'cors allow methods': false,
        'cors allow headers': false,

        'custom engine': engine({
            helpers: [
                handlebarsHelpers,
                templateHelpers,
                helperMoment
            ],
            layoutsDir: 'views/layouts',
            partialsDir: 'views/partials',
            defaultLayout: 'index.handlebars'
        }),
        'view engine': 'handlebars',
    },
    policies: {
        directives: {
            defaultSrc: ["'self'"],
            childSrc: ["'self'", 'https://*.youtube-nocookie.com', 'https://maps.google.com', 'https://www.google.com', 'https://calendar.google.com'],
            styleSrc: ["'self'", "'unsafe-inline'", 'https://fonts.googleapis.com'],
            fontSrc: ["'self'", 'https://fonts.googleapis.com', 'https://barra.brasil.gov.br', 'https://fonts.gstatic.com'],
            imgSrc: ["'self'", "data:", 'https://barra.brasil.gov.br'],
            scriptSrc: ["'strict-dynamic'"],
            objectSrc: ["'none'"],
            upgradeInsecureRequests: true,
            baseUri: ["'self'"],
        },
        browserSniff: false,
    },
    hsts: {
        maxAge: 5184000,
        includeSubDomains: false,
    },
    expectCT: {
        enforce: true,
        maxAge: 120,
    },
    featurePolicy: {
        features: {
            fullscreen: ["'self'"],
        }
    },
    portal: {},
    models: './models',
    routes: './routes',
}